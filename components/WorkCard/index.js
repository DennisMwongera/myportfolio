import React from "react";

const WorkCard = ({ img, name, description, onClick }) => {
  return (
    <div
      className="overflow-hidden bg-gray-300 rounded-lg p-2 laptop:p-4 first:ml-0 link"
      onClick={onClick}
    >
      <div
        className="relative rounded-md overflow-hidden transition-all ease-out duration-300 h-48 mob:h-auto"
        style={{ height: "100px" }}
      >
        <img
          className="h-50 w-50 object-cover hover:opacity-90 transition-all ease-out duration-300"
          src={img}
        ></img>
      </div>
      <h1 className="mt-5 text-2xl text-black font-medium">
        {name ? name : "Project Name"}
      </h1>
      <h2 className="text-lg text-black opacity-50">
        {description ? description : "Description"}
      </h2>
    </div>
  );
};

export default WorkCard;

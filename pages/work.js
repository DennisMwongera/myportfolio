import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Cursor from "../components/Cursor";
import Header from "../components/Header";
import WorkCard from "../components/WorkCard";
// import ProjectResume from "../components/ProjectResume";
// import Socials from "../components/Socials";
import Button from "../components/Button";
import { useTheme } from "next-themes";
// Data
import { name, work } from "../data/portfolio.json";
import { resume } from "../data/portfolio.json";
import data from "../data/portfolio.json";

const Work = () => {
  const router = useRouter();
  const theme = useTheme();
  const [mount, setMount] = useState(false);

    // Ref
    // const workRef = useRef();

  useEffect(() => {
    setMount(true);
    if (!work) {
      router.push("/");
    }
  }, []);
  return (
    <>
      {process.env.NODE_ENV === "development" && (
        <div className="fixed bottom-6 right-6">
          <Button onClick={() => router.push("/edit")} type={"primary"}>
            Edit Resume
          </Button>
        </div>
      )}
      {data.showCursor && <Cursor />}
      <div
        className={`container mx-auto mb-10 ${
          data.showCursor && "cursor-none"
        }`}
      >
        <Header isBlog />
        {mount && (
             <div className="mt-10 laptop:mt-30 p-2 laptop:p-0">
             <h1 className="text-2xl text-bold">Work.</h1>
   
             <div className="mt-5 laptop:mt-10 grid grid-cols-1 tablet:grid-cols-2 gap-2">
               {data.projects.map((project) => (
                 <WorkCard
                   key={project.id}
                  //  img={project.imageSrc}
                   name={project.title}
                   description={project.description}
                   onClick={() => window.open(project.url)}
                 />
               ))}
             </div>
           </div>
        )}
      </div>
    </>
  );
};

export default Work;
